# Scratch Privacy Hub
Source code for Scratch Privacy Hub version 3.0

## Licensing
The source code is availible under GNU GPL 3.0.

## What is Scratch Privacy Hub?
Scratch Privacy Hub is a Scratch project created by themirrazz's Mirrazz Privacy Group.
Scratch Privacy Hub allows users on Scratch to easily see how Scratch projects handle their data, divided into sections like on the Apple App Store.

## Where can I find the project?
Scratch Privacy Hub 3.0 has just been released, you can find it [here](https://scratch.mit.edu/projects/743550521).
