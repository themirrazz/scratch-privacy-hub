const Scratch = require("scratch3-api");
const fs=require('node:fs/promises');

/*
When personalization is turned on,
projects from these people will show up
if you're following them.
*/
var scratchers=[
    'griffpatch',
    'Will_Wam',
    'TimMcCool',
    'World_Languages',
    'crater24'
]

async function main() {
    let session=await Scratch.UserSession.create(
        'crater24',
        process.env["password"]
    );
    let cloud=await Scratch.CloudSession.create(
        session,
        743550521
    );
    cloud.connect();
    cloud.on('set', async (name,value)=>{
        if(name===cloud.name("client_request")&&value!=0) {
            var data=cloud.stringify(value);
            cloud.set(cloud.name("client_request"),0);
            var username=data.split("@")[0];
            var req=data.split("@")[1];
            var prefs=JSON.parse(
                await fs.readFile('preferences.json',{
                    encoding:'utf-8'
                })
            );
            if(!prefs[username]) {
                prefs[username]={
                    theme: 'dark',
                    personalization:0,
                    mobilekeyboard: 0
                }
            }
            if(req==='dark') {
                prefs[username].theme='dark'
            } else if(req==='light') {
                prefs[username].theme='light'
            } else if(req==='mkon') {
                prefs[username].mobilekeyboard=true
            } else if(req==='mkoff') {
                prefs[username].mobilekeyboard=false
            } else if(req==='gpref') {
                var f=await Scratch.Rest.getFollowing();
                var d="";
                scratchers.forEach(e=>{
                    if(f.includes(e)) {
                        d+='1'
                    } else {
                        d+='0'
                    }
                });
                cloud.set(
                    cloud.name("server_response"),
                    cloud.stringify(
                        `${username}@prefs:${d}${prefs[username].theme==='dark'?'1':'0'}${prefs[username].mobilekeyboard?'1':'0'}`
                    )
                )
            }
        }
    })
}

main()
